var about = $("#about-text").position().top;
var upcoming = $("#upcoming-text").position().top;
var contact = $("#contact-text").position().top;

$("#about-button").click(function() {
    $('html,body').animate({
        scrollTop: about -180
    },'slow');
});

$("#upcom-button").click(function() {
    $('html,body').animate({
        scrollTop: upcoming -180
    },'slow');
});

$("#contact-button").click(function() {
    $('html,body').animate({
        scrollTop: contact -180
    },'slow');
});

var $w = $(window).scroll(function(){
    var logoContainer = $('#logo');
    var logo = $('#logo img');
    var nav = $('#navigation');
    if ( $w.scrollTop() > 200 ) {
        logoContainer.addClass("yellow-backdrop");
        nav.addClass("drop-shadow");
        logoContainer.css({
            position: "fixed",
        });
        logo.css({
            marginTop: "0"
        });
        nav.css({
            position: "fixed",
            marginTop: "197px"
        });
    } else {
        logoContainer.removeClass("yellow-backdrop");
        nav.removeClass("drop-shadow");
        logoContainer.css({

            position: "absolute",
        });
        logo.css({
            marginTop: "150px"
        });
        nav.css({

            position: "absolute",
            marginTop: "350px"

        });
    }
});

window.onbeforeunload = function(){
	window.scrollTo(0,0);
};

$(document).ready(function(){
    $('li.active').removeClass('active');
    $('#navigation ul li').on('click', function(){
        $('li.active').removeClass('active');
        $(this).addClass('active');
    });
});
