var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    livereload = require('gulp-livereload'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function()
{
    return gulp.src('./resources/assets/sass/main.scss')
        .pipe(sass({
            style: 'compressed',
            includePaths: [
                './node_modules/support-for/sass'
            ]
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(gulp.dest('./public/css'))
        .pipe(livereload());
});

gulp.task('lib', function()
{
    gulp.src([
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/vue/vue.min.js'
        ])
        .pipe(uglify())
        .pipe(concat('lib.min.js'))
        .pipe(gulp.dest('./public/js'));
    return livereload.reload();
});

gulp.task('js', function()
{
    gulp.src(['./resources/assets/js/**/_*.js', './resources/assets/js/main.js'])
        //.pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest('./public/js'));

    return livereload.reload();
});

gulp.task('watch', function()
{
    livereload.listen();
    gulp.watch('./resources/assets/sass/**/*.scss', ['sass']);
    gulp.watch('./resources/assets/js/**/*.js', ['js']);
});

gulp.task('default', ['sass', 'lib', 'js']);
