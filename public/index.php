<?php include('partials/header.php'); ?>

<div class="container">

    <span class="anchors" id="abt"></span>

    <div id="about-text" class="page-section">
        <h1 class="heading">
            ABOUT
        </h1>
        <h2>
            23 April 2016
        </h2>
        <h3>
            LAERSKOOL LOUW GELDENHUYS<br>
            3RD AVENUE &amp; 7TH STREET, LINDEN<br><br>

            Entrance R100 (PRE-SCHOOL CHILDREN FREE)<br><br>

            TICKETS AVAILABLE AT GKL OFFICES<br>
            filia@linden.org.za • 011 888-5735<br>
            or Computicket (GIG 2016)<br>
        </h3>
        <div class="row">
            <div class="gr-3 gr-0@mobile"></div>
            <div id="about-content" class="gr-6 gr-12@mobile">
                <p>
                    GiG 2016  -  Op 23 April 2016 word die 2e Lofprysings
                    geleentheid GiG (God is Groot/God is Great) in Linden,
                    Johannesburg aangebied.  GiG is 'n jaarlikse geleentheid
                    waar Christene van alle kerke en denominasies bymekaarkom
                     om God se Naam te loof en prys. Die dag sluit welbekende
                     aanbidders soos Retief Burger, Joe Niemand, Helene Bester
                     en Joshua na die Reën in.  Dan is daar ook 'n verassings
                     Jeugsanggroep vir tieners en jonger kinders. Almal kom
                     saam vir die uitsluitelike doel van Lofprysing.
                </p>
            </div>
            <div class="gr-3 gr-0@mobile"></div>
        </div>
        <div id="download" class="row">
            <span class="gr-1 gr-0@mobile"></span>
            <a class="gr-3 gr-12@mobile" href="downloads/GIG_2016-RetiefBurger.pdf" target="_blank">
                Retief Burger
            </a>
            <a class="gr-3 gr-12@mobile" href="downloads/GiG2016-Program-InligtingInformation.pdf" target="_blank">
                Program &amp; Inligting / Information
            </a>
            <a class="gr-3 gr-12@mobile" href="downloads/GIG2016-ProgramandSongs_ImagineYouthBand.pdf" target="_blank">
                #Imagine Youth Band songs
            </a>
        </div>
    </div>


    <span class="anchors" id="upcm"></span>

    <div id="upcoming-text" class="page-section">
        <h1 class="heading">
            UPCOMING EVENTS
        </h1>
        <h3>
            <span class="dates">23 April 2016</span> <span class="event">Praise and Worship</span> Laerskool Louw Geldenhuys
        </h3>
    </div>

    <span class="anchors" id="cntct"></span>

    <div id="contact-text" class="page-section">
        <h1 class="heading">
            CONTACT
        </h1>
        <div class="row">
            <span class="gr-3 gr-0@mobile"></span>
            <h3 class="gr-3 gr-12@mobile">
                <span class="bolded">Email:</span> filia@linden.org.za<br>
                <span class="bolded">Telephone:</span> 011 888-5735
            </h3>
            <h3 class="gr-3 gr-12@mobile">
                <span class="bolded">
                    Laerskool Louw Geldenhuys<br>
                    3rd Avenue &amp; 7th Street, Linden
                </span>
            </h3>
            <span class="gr-3 gr-0@mobile"></span>
        </div>
        <div class="row">
            <span class="gr-2 gr-1@mobile"></span>
            <span class="gr-8 gr-10@mobile">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14327.223879368728!2d27.9952294!3d-26.1378687!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x98e2f89a017b9241!2sLouw+Geldenhuys+Skool!5e0!3m2!1sen!2s!4v1461239328181" frameborder="0" style="border:0" allowfullscreen></iframe>
            </span>
            <span class="gr-2 gr-1@mobile"></span>
        </div>
    </div>
</div>


<?php include('partials/footer.php'); ?>
