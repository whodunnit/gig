<!doctype html>
<html>
<head>
    <title>GIG Praise &amp; Worship</title>
    <meta name="GIG" content="GIG Praise & Worship">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="IE=Edge">

    <link href="css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,900|Yesteryear' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="front-page">

        <div id="logo">
            <img id="main-logo" src="images/logo.jpg">
            <img id="small-logo" src="images/logo-small.jpg">
        </div>
        <div id="navigation" class="row">
            <ul>
                <li class="button active" id="about-button">about</li>
                <li class="button" id="upcom-button">events</li>
                <li class="button" id="contact-button">contact</li>
            </ul>
        </div>
        <span id="page-top"></span>
    </div>
